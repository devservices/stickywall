/* global oCanvas, bootbox, TogetherJS, BackboneEvents */
"use strict";

/*
 * Wall
 */

var Wall = function(canvas, textEdit, width, height) {
    var self = this;

    self.canvas = oCanvas.create({canvas: canvas, background: 'white'});
    self.canvas.width = width;
    self.canvas.height = height;

    self.$textEdit = $(textEdit);

    // an invisible object that is used to capture clicks on the canvas itself
    // (clicks on other objects bubble to the canvas object)
    self.surface = self.canvas.display.rectangle({
        x: 0,
        y: 0,
        height: height,
        width: width
    });
    self.canvas.addChild(self.surface);

    self.trash = self.canvas.display.image({
        x: 10,
        y: height,
        height: 55,
        width: 55,
        origin: { x: "left", y: "bottom" },
        image: "img/trash.png"
    });
    self.canvas.addChild(self.trash);

    self.currentTool = null;
    self.tools = {};

    self.children = {};

    // TODO: Check permissions in handlers

    self.surface
    .bind("click tap", function (e) {
        if(self.currentTool !== null) {
            if(self.currentTool.click(e)) {
                self.canvas.redraw();
            }
        }
    })
    .bind("dblclick dbltap", function (e) {
        if(self.currentTool !== null) {
            if(self.currentTool.doubleClick(e)) {
                self.canvas.redraw();
            }
        }
    })
    .bind("mousedown touchstart", function (e) {
        if(self.currentTool !== null) {
            if(self.currentTool.mouseDown(e)) {
                self.canvas.redraw();
            }
        }
    })
    .bind("mousemove touchmove", function (e) {
        if(self.currentTool !== null) {
            if(self.currentTool.mouseMove(e)) {
                self.canvas.redraw();
            }
        }
    })
    .bind("mouseup touchend", function (e) {
        if(self.currentTool !== null) {
            if(self.currentTool.mouseUp(e)) {
                self.canvas.redraw();
            }
        }
    });
};

Wall.prototype = _.extend(Object.create(null), BackboneEvents, {

    add: function(element, x, y) {
        if(x !== undefined && y !== undefined) {
            element.container.x = x;
            element.container.y = y;
        }

        this.children[element.container.id] = element;
        this.canvas.addChild(element.container);

        this.trigger("added", element);
    },

    remove: function(element) {
        delete this.children[element.container.id];
        this.canvas.removeChild(element.container);

        this.trigger("removed", element);
    },

    update: function(element) {
        this.canvas.redraw();
        this.trigger('updated', element);
    },

    clear: function() {
        var self = this;

        _.each(self.children, function(element) {
            self.canvas.removeChild(element.container, false);
        });

        self.children = {};
        self.canvas.redraw();

        self.trigger('cleared');
    },

    registerTool: function(tool) {
        this.tools[tool.type] = tool;
    },

    enableTool: function(tool) {
        this.currentTool = this.tools[tool];
        this.trigger("toolChanged", this.currentTool );
    },

    disableTool: function() {
        this.currentTool = null;
        this.trigger("toolChanged", null);
    }

});

/*
 * Wall element prototype
 */

var WallElementPrototype = _.extend({}, BackboneEvents, {

    tool: null,
    container: null,

    // Override to manage save/load/patch for collaboration
    serialize: function() {
        return {};
    },

    update: function(data) {
    }

});

/*
 * Sticky note
 */

var Sticky = function(tool, options) {
    var self = this;

    this.tool = tool;

    options = _.defaults(options || {}, {
        text: '',
        color: 'yellow',
        textColor: 'black',
        borderColor: '#ddd'
    });

    this.container = self.tool.wall.canvas.display.rectangle({
        x: 0,
        y: 0,
        width: 140,
        height: 100,
        fill: options.color,
        stroke: '1px ' + options.borderColor,
        strokePosition: "inside"
    });

    this.label = self.tool.wall.canvas.display.text({
        x: 70,
        y: 50,
        width: 140,
        height: 100,
        origin: { x: "center", y: "center" },
        font: "14px 'Helvetica Neue', Helvetica, Arial, sans-serif",
        fill: options.textColor,
        align: "center",
        text: options.text
    });

    this.container.addChild(this.label);
};

Sticky.prototype = _.extend(Object.create(WallElementPrototype), {

    setText: function(text) {
        this.label.text = text;
        this.tool.wall.update(this);
    },

    setColor: function(color) {
        this.container.fill = color;
        this.tool.wall.update(this);
    },

    serialize: function() {
        return {
            x: this.container.x,
            y: this.container.y,
            color: this.container.fill,
            text: this.label.text
        };
    },

    update: function(data) {
        this.container.x = data.x;
        this.container.y = data.y;
        this.container.fill = data.color;
        this.label.text = data.text;
    }

});


/*
 * Text box
 */

var TextBox = function(tool, options) {
    var self = this;

    this.tool = tool;

    options = _.defaults(options || {}, {
        text: "",
        color: 'black'
    });

    this.container = self.tool.wall.canvas.display.text({
        origin: { x: "left", y: "top" },
        align: 'left',
        font: "16pt sans-serif",
        fill: options.color,
        text: options.text
    });
};

TextBox.prototype = _.extend(Object.create(WallElementPrototype), {

    setText: function(text) {
        this.container.text = text;
        this.tool.wall.update(this);
    },

    setColor: function(color) {
        this.container.fill = color;
        this.tool.wall.update(this);
    },

    serialize: function() {
        return {
            x: this.container.x,
            y: this.container.y,
            color: this.container.fill,
            text: this.container.text
        };
    },

    update: function(data) {
        this.container.x = data.x;
        this.container.y = data.y;
        this.container.fill = data.color;
        this.container.text = data.text;
    }
});

/*
 * Line
 */

var Line = function(tool, options) {
    var self = this;

    this.tool = tool;

    options = _.defaults(options || {}, {
        start: {x: 0, y: 0},
        end: {x: 0, y: 0},
        width: 2,
        color: 'black',
    });

    this.width = options.width;
    this.color = options.color;

    this.container = self.tool.wall.canvas.display.line({
        start: options.start,
        end: options.end,
        stroke: options.width + "px " + options.color,
        cap: "round"
    });
};



Line.prototype = _.extend(Object.create(WallElementPrototype), {

    setStart: function(start) {
        this.container.start = start;
        this.tool.wall.update(this);
    },

    setEnd: function(end) {
        this.container.end = end;
        this.tool.wall.update(this);
    },

    serialize: function() {
        return {
            start: this.container.start,
            end: this.container.end,
            width: this.width,
            color: this.color
        };
    },

    update: function(data) {
        this.container.start = data.start;
        this.container.end = data.end;

        var width = data.width || this.width,
            color = data.color || this.color;

        this.container.stroke = width + "px " + color;
    }

});

/*
 * Tool prototype
 */

var WallToolPrototype = _.extend({}, BackboneEvents, {

    wall: null,

    create: function(options) {
        return null;
    },

    // event delgates; return true to cause canvas to be redrawn after

    click: function(event) {
        return false;
    },

    doubleClick: function(event) {
        return false;
    },

    mouseDown: function(event) {
        return false;
    },

    mouseMove: function(event) {
        return false;
    },

    mouseUp: function(event) {
        return false;
    },

    enableStandardMouseActions: function(element) {
        var self = this;

        // TODO: Check permissions in handlers
        element.container.dragAndDrop({
            changeZindex: true,
            move: function() {
                self.wall.update(element);

                if(self.wall.trash.isPointerInside() && element.container.opacity === 1) {
                    element.container.fadeTo(0.3);
                } else if(!self.wall.trash.isPointerInside() && element.container.opacity < 1) {
                    element.container.opacity = 1;
                    element.container.redraw();
                }

            },
            end: function() {
                if(self.wall.trash.isPointerInside()) {
                    self.wall.remove(element);
                }
            }
        })
        .bind("mouseenter", function () {
            self.wall.canvas.mouse.cursor("move");
        })
        .bind("mouseleave", function () {
            self.wall.canvas.mouse.cursor("default");
        });
    }

});

/*
 * Sticky tool
 */

var StickyTool = function(wall, color) {
    this.wall = wall;
    this.color = color;
    this.editing = false;
};

StickyTool.prototype = _.extend(Object.create(WallToolPrototype), {

    type: 'sticky',

    create: function(options) {
        var self = this;

        var sticky = new Sticky(self, _.defaults(options || {}, {
            color: self.color
        }));

        self.enableStandardMouseActions(sticky);

        // TODO: Check permissions in handlers
        sticky.container.bind("dblclick", function(e) {
            self.editing = true;

            self.wall.$textEdit
            .css({
                left: sticky.container.x,
                top: sticky.container.y,
                height: sticky.container.height,
                width: sticky.container.width,
                'font-size': '14px',
                'text-align': 'center',
                'color': '#333',
                'background-color': sticky.container.color
            })
            .val(sticky.label.text)
            .show()
            .focus()
            .one('blur', function() {
                var text = $(this).val();

                if(text) {
                    sticky.setText(text);
                }

                $(this).hide();

                // avoid a click on the canvas causing a new sticky to be created
                setTimeout(function() { self.editing = false; }, 100);
                self.trigger('end');
            });
        });

        return sticky;
    },

    setColor: function(color) {
        this.color = color;
    },

    click: function(e) {
        var self = this;

        if(self.editing) {
            return false;
        }

        self.editing = true;

        var x = e.x, y = e.y;

        self.wall.$textEdit
        .css({
            left: x,
            top: y,
            width: '140px',
            height: '100px',
            'font-size': '14px',
            'text-align': 'center',
            'color': '#333',
            'background-color': self.color
        })
        .val("")
        .show()
        .focus()
        .one('blur', function() {
            var text = $(this).val();

            if(text) {
                var sticky = self.create({text: text});
                self.wall.add(sticky, x, y);
            }

            $(this).hide();

            // avoid a click on the canvas causing a new sticky to be created
            setTimeout(function() { self.editing = false; }, 100);
            self.trigger('end');
        });

        // adding the element causes a redraw anyway
        return false;
    }
});

/*
 * Text box tool
 */

var TextTool = function(wall, color) {
    this.wall = wall;
    this.color = color;
    this.editing = false;
};

TextTool.prototype = _.extend(Object.create(WallToolPrototype), {

    type: 'text',

    create: function(options) {
        var self = this;

        var textBox = new TextBox(self, _.defaults(options || {}, {
            color: self.color
        }));

        self.enableStandardMouseActions(textBox);

        // TODO: Check permissions in handlers
        textBox.container.bind("dblclick", function() {

            self.wall.$textEdit
            .css({
                left: textBox.container.x,
                top: textBox.container.y,
                width: textBox.container.width + 15,
                height: textBox.container.height + 20,
                'font-size': '16px',
                'text-align': 'left',
                'color': textBox.container.fill,
                'background-color': 'white'
            })
            .val(textBox.container.text)
            .show()
            .focus()
            .one('blur', function() {
                var text = $(this).val();

                if(text) {
                    textBox.setText(text);
                }

                $(this).hide();

                // avoid a click on the canvas causing a new text box to be created
                setTimeout(function() { self.editing = false; }, 100);
                self.trigger('end');
            });

        });

        return textBox;
    },

    setColor: function(color) {
        this.color = color;
    },

    click: function(e) {
        var self = this;

        if(self.editing) {
            return false;
        }

        self.editing = true;

        var x = e.x, y = e.y;

        self.wall.$textEdit
        .css({
            left: x,
            top: y,
            width: '180px',
            height: '24px',
            'font-size': '16px',
            'text-align': 'left',
            'color': self.color,
            'background-color': 'white'
        })
        .val("")
        .show()
        .focus()
        .one('blur', function() {
            var text = $(this).val();

            if(text) {
                var textBox = self.create({text: text});
                self.wall.add(textBox, x, y);
            }

            $(this).hide();

            // avoid a click on the canvas causing a new text box to be created
            setTimeout(function() { self.editing = false; }, 100);
            self.trigger('end');
        });

        // adding the element causes a redraw anyway
        return false;
    }
});


/*
 * Line tool
 */

var LineTool = function(wall, color, width) {
    this.wall = wall;

    this.line = null;
    this.editMode = false;

    this.color = color;
    this.width = width;
};

LineTool.prototype = _.extend(Object.create(WallToolPrototype), {

    type: 'line',

    create: function(options) {
        var self = this;

        var line = new Line(this, _.defaults(options || {}, {
            width: this.width,
            color: this.color
        }));

        this.enableStandardMouseActions(line);

        // TODO: Check permissions in handlers
        line.container.bind("dblclick", function(e) {

            self.wall.enableTool(self.type);
            self.wall.currentTool.line = line;
            self.wall.currentTool.editMode = true;

            line.setEnd({x: e.x, y: e.y});

            // temporarily bind events to the line that won't bubble to the
            // surface

            var mouseMoveHandler = function(ex) {
                self.mouseMove(ex);
            };
            var mouseUpHandler = function(ex) {
                self.mouseUp(ex);
            };

            line.container.bind('mousemove touchmove', mouseMoveHandler);
            line.container.bind('mouseup touchend', mouseUpHandler);

            self.once('end', function() {
                line.container.unbind('mousemove touchmove', mouseMoveHandler);
                line.container.unbind('mouseup touchend', mouseUpHandler);
            });

        });

        return line;
    },

    setColor: function(color) {
        this.color = color;
    },

    setWidth: function(width) {
        this.width = width;
    },

    mouseDown: function(e) {
        var self = this;

        if(self.line === null) {
            self.editMode = false;

            var line = self.line = self.create({
                start: {x: e.x, y: e.y},
                end: {x: e.x + 1, y: e.y + 1 }
            });

            // temporarily bind events to the line that won't bubble to the
            // surface

            var mouseMoveHandler = function(ex) {
                self.mouseMove(ex);
            };
            var mouseUpHandler = function(ex) {
                self.mouseUp(ex);
            };

            line.container.bind('mousemove touchmove', mouseMoveHandler);
            line.container.bind('mouseup touchend', mouseUpHandler);

            self.once('end', function() {
                line.container.unbind('mousemove touchmove', mouseMoveHandler);
                line.container.unbind('mouseup touchend', mouseUpHandler);
            });

            self.wall.add(this.line);
            return true;
        } else {
            return false;
        }
    },

    mouseMove: function(e) {
        if(this.line !== null) {
            this.line.setEnd({x: e.x, y: e.y});

            return true;
        } else {
            return false;
        }
    },

    mouseUp: function(e) {
        this.trigger('end');
        if(this.line !== null) {
            this.line = null;
            return true;
        } else {
            return false;
        }
    }
});

/*
 * Template
 */


Template.canvas.rendered = function() {
    var template = this;

    var wall = new Wall('#canvas', '#text-edit', 2500, template.$('#canvas-container').height() - 5);

    wall.registerTool(new StickyTool(wall, '#fcf0ad'));
    wall.registerTool(new LineTool(wall, '#333', 2));
    wall.registerTool(new TextTool(wall, '#333'));

    wall.on('toolChanged', function(tool) {
        var active = '.tool-select';

        if(tool !== null) {
            active = '.tool-' + tool.type;
        }

        template.$('.tools .tool-enable').removeClass('active');
        template.$(active + ' .tool-enable').addClass('active');
    });

    // Buttons

    template.$(".tool-select .tool-enable").click(function() {
        wall.disableTool();
    });

    template.$(".tool-sticky .tool-enable").click(function(e) {
        e.preventDefault();

        wall.enableTool('sticky');
    });

    template.$(".tool-sticky .tool-color a").click(function(e) {
        e.preventDefault();

        var $li = $(this).parent("li"),
            $ul = $li.parent("ul"),
            color = $li.data("color");

        wall.tools.sticky.setColor(color);
        $ul.find(".active").removeClass('active');
        $li.addClass('active');

        wall.enableTool('sticky');
    });

    template.$(".tool-line .tool-enable").click(function() {
        wall.enableTool('line');
    });

    template.$(".tool-line .tool-color a").click(function(e) {
        e.preventDefault();

        var $li = $(this).parent("li"),
            $ul = $li.parent("ul"),
            color = $li.data("color");

        wall.tools.line.setColor(color);
        $ul.find(".active").removeClass('active');
        $li.addClass('active');

        wall.enableTool('line');
    });

    template.$(".tool-text .tool-enable").click(function() {
        wall.enableTool('text');
    });

    template.$(".tool-text .tool-color a").click(function(e) {
        e.preventDefault();

        var $li = $(this).parent("li"),
            $ul = $li.parent("ul"),
            color = $li.data("color");

        wall.tools.text.setColor(color);
        $ul.find(".active").removeClass('active');
        $li.addClass('active');

        wall.enableTool('text');
    });

    template.$(".clear").click(function(e) {
        e.preventDefault();

        bootbox.confirm("This will delete all items on the wall. Are you sure?", function(confirm) {
            if(confirm) {
                wall.clear();
            }
        });
    });

    // Collaboration

    template.$(".button-sharing li a").click(function(e) {
        e.preventDefault();

        var $li = $(this).parent("li"),
            $ul = $li.parent("ul"),
            $group = $ul.parent(".btn-group"),
            $label = $group.find(".button-text");

        var currentMode = $li.data('mode');

        $label.html($(this).html());
        $ul.find("li").show();
        $li.hide();

        if(window.TogetherJS !== undefined && (currentMode === 'read-write' || currentMode === 'read-only')) {
            template.$('.button-collaborate').show();
        } else {
            template.$('.button-collaborate').hide();
        }

    });

    if(window.TogetherJS !== undefined) {

        template.$(".collaborate").click(function() {
            TogetherJS();
        });

        TogetherJS.hub.on("hello", function(msg) {
            if(!msg.sameUrl) {
                return;
            }
            // TODO
        });

        wall.on("added", function(element) {
            if(TogetherJS.running) {
                    TogetherJS.send({
                    type: "added",
                    id: element.container.id,
                    elementType: element.tool.type,
                    data: element.serialize()
                });
            }
        });
        TogetherJS.hub.on("added", function(msg) {
            var element = wall.children[msg.id];
            if(!element) {
                var tool = wall.tools[msg.elementType];
                if(!tool) {
                    return;
                }

                var newElement = tool.create();
                newElement.update(msg.data);

                wall.add(newElement);
            }
        });

        wall.on("removed", function(element) {
            if(TogetherJS.running) {
                    TogetherJS.send({
                    type: "removed",
                    id: element.container.id
                });
            }
        });
        TogetherJS.hub.on("removed", function(msg) {
            var element = wall.children[msg.id];
            if(element) {
                wall.remove(element);
            }
        });

        wall.on("updated", function(element) {
            if(TogetherJS.running) {
                    TogetherJS.send({
                    type: "updated",
                    id: element.container.id,
                    elementType: element.tool.type,
                    data: element.serialize()
                });
            }
        });
        TogetherJS.hub.on("updated", function(msg) {
            var element = wall.children[msg.id];
            if(element) {
                element.update(msg.data);
                wall.canvas.redraw();
            }
        });

        wall.on("cleared", function(element) {
            if(TogetherJS.running) {
                    TogetherJS.send({
                    type: "cleared"
                });
            }
        });
        TogetherJS.hub.on("removed", function(msg) {
            wall.clear();
        });
    } else {
        template.$('.button-collaborate').hide();
    }

};