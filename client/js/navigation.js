/* global Collections, Models, Roles */
"use strict";

Template.header.helpers({
    isAdmin: function() {
        return Roles.userIsInRole(Meteor.user(), ['admin']);
    },
    adminAccountsActive: function() {
        var currentRoute = Router.current();
        return currentRoute && currentRoute.route.name === "adminAccounts"? "active" : null;
    },
});
