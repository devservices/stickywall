/* global Collections, Models, bootbox, Roles */
"use strict";

Router.map(function() {

    var go = Router.go;
    Router.go = function () {
        var self = this,
            args = arguments;
        if(Session.get('dirty')) {
            bootbox.confirm("You have unsaved changes, which will be lost if you navigate away. Really leave this page?", function(confirm) {
                if(confirm) {
                    go.apply(self, args);
                }
            });
        } else {
            go.apply(self, args);
        }
    };

    Router.configure({
        layoutTemplate: 'layout',
        notFoundTemplate: 'notFound',

        onBeforeAction: function(pause) {
            var self = this;

            if(!this.ready()) {
                return;
            }

            var userId = Meteor.userId(),
                currentRoute = Router.current();

            // Redirect to home page if user is not logged in
            if(currentRoute && currentRoute.route.name !== 'home' && !userId) {
                pause();
                Router.go('home');
                return;
            }

            Session.set('dirty', false);
        }
    });

    Router.onBeforeAction('dataNotFound');
    Router.onBeforeAction('loading');

    this.route('home', {
        path: '/',
        data: {}
    });

    this.route('adminAccounts', {
        path:'/admin/accounts',
        template: 'adminAccounts',
        data: {},
        onBeforeAction: function() {
            if(Meteor.loggingIn()) {
                this.render(this.loadingTemplate);
            } else if(!Roles.userIsInRole(Meteor.user(), ['admin'])) {
                this.redirect('/');
            }
        }
    });

    this.route('newCanvas', {
        path:'/new',
        template: 'canvas',
        data: {}
    });

});
